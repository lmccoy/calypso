# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#!/usr/bin/env python
import sys
from AthenaCommon.Constants import VERBOSE, INFO
from AthenaConfiguration.ComponentFactory import CompFactory

def GeoModelTestCfg(flags, name="GeoModelTestAlg", **kwargs):

    # Initialize GeoModel
    from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
    a = FaserGeometryCfg(flags)

    # Initialize field service
    from MagFieldServices.MagFieldServicesConfig import MagneticFieldSvcCfg
    a.merge(MagneticFieldSvcCfg(flags))
    # Disable the field
    cacheAlg = a.getCondAlgo("FaserFieldCacheCondAlg")
    cacheAlg.UseDipoScale = 0.0
    cacheAlg.UseDCS = False

    # Configure the algorithm itself
    GeoModelTestAlg = CompFactory.GeoModelTestAlg
    a.addEventAlgo(GeoModelTestAlg(name, FirstSCTStation=0, 
                                         LastSCTStation=0,
                                         NumVetoStations=1, 
                                         NumVetoPlatesPerStation=2,
                                         NumVetoNuStations=0,
                                         NumTriggerStations=0,
                                         PrintSctIDs=True,
                                         **kwargs))

    return a

if __name__ == "__main__":
    from AthenaCommon.Logging import log#, logging
    from AthenaCommon.Configurable import Configurable
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags

    Configurable.configurableRun3Behavior = True
    
# Flags for this job
    ConfigFlags.Input.isMC = True                                # Needed to bypass autoconfig
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-TB00"             # Always needed; must match FaserVersion
    ConfigFlags.GeoModel.FaserVersion     = "FASER-TB01"           # Default FASER geometry
    # ConfigFlags.GeoModel.GeoExportFile    = "faserTestBeamGeo.db"        # Writes out a GeoModel file with the full geometry tree (optional, comment out to skip)
    # ConfigFlags.Detector.EnableVeto     = True
    # ConfigFlags.Detector.EnableTrigger  = True
    # ConfigFlags.Detector.EnablePreshower= True
    # ConfigFlags.Detector.EnableFaserSCT = True
    # ConfigFlags.Detector.EnableUpstreamDipole = True
    # ConfigFlags.Detector.EnableCentralDipole = True
    # ConfigFlags.Detector.EnableDownstreamDipole = True
    # ConfigFlags.Detector.EnableEcal = True
    ConfigFlags.lock()

# Configure components
    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(ConfigFlags)

# Set up algorithm
    acc.merge(GeoModelTestCfg(ConfigFlags))

# Configure verbosity    
    msgSvc = acc.getService("MessageSvc")
    msgSvc.Format = "% F%30W%S%7W%R%T %0W%M"
    # ConfigFlags.dump()
    # logging.getLogger('forcomps').setLevel(VERBOSE)
    acc.foreach_component("*").OutputLevel = VERBOSE
    acc.foreach_component("*ClassID*").OutputLevel = INFO
    log.setLevel(VERBOSE)
    
# Execute and finish
    sys.exit(int(acc.run(maxEvents=1).isFailure()))
