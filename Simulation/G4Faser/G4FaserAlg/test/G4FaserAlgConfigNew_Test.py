#!/usr/bin/env python
"""Run tests on G4FaserAlgConfigNew

Copyright (C) 2002-2021 CERN for the benefit of the ATLAS and FASER collaborations
"""

if __name__ == '__main__':

    import time
    a = time.time()
#
# Set up logging and config behaviour
#
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG, VERBOSE
    from AthenaCommon.Configurable import Configurable
    log.setLevel(DEBUG)
    Configurable.configurableRun3Behavior = 1
#
# Import and set config flags
#
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Exec.MaxEvents = 4  # can be overridden from command line with --evtMax=<number>
    ConfigFlags.Exec.SkipEvents = 0 # can be overridden from command line with --skipEvents=<number>
    from AthenaConfiguration.Enums import ProductionStep
    ConfigFlags.Common.ProductionStep = ProductionStep.Simulation
#
# All these must be specified to avoid auto-configuration
#
    ConfigFlags.Input.RunNumber = [12345] #Isn't updating - todo: investigate
    ConfigFlags.Input.OverrideRunNumber = True
    ConfigFlags.Input.LumiBlockNumber = [1]
    ConfigFlags.Input.isMC = True
#
# Output file name
# 
    ConfigFlags.Output.HITSFileName = "my.HITS.pool.root" # can be overridden from command line with Output.HITSFileName=<name>
#
# Sim ConfigFlags
#
    ConfigFlags.Sim.Layout = "FASER"
    ConfigFlags.Sim.PhysicsList = "FTFP_BERT"
    ConfigFlags.Sim.ReleaseGeoModel = False
    ConfigFlags.Sim.IncludeParentsInG4Event = True # Controls whether BeamTruthEvent is written to output HITS file
    ConfigFlags.addFlag("Sim.Gun",{"Generator" : "SingleParticle"})  # Property bag for particle gun keyword:argument pairs
    ConfigFlags.addFlag("Sim.Beam.xangle", 0)  # Potential beam crossing angles
    ConfigFlags.addFlag("Sim.Beam.yangle", 0)
    ConfigFlags.addFlag("Sim.Beam.xshift", 0)  # Potential beam shift
    ConfigFlags.addFlag("Sim.Beam.yshift", 0)        

    ConfigFlags.GeoModel.FaserVersion = "FASERNU-04"             # Geometry set-up
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-03"             # Conditions set-up
    ConfigFlags.addFlag("Input.InitialTimeStamp", 0)             # To avoid autoconfig 
    ConfigFlags.GeoModel.Align.Dynamic = False
#
# Override flags above from command line
#
    import sys
    ConfigFlags.fillFromArgs(sys.argv[1:])

    doShiftLOS = (ConfigFlags.Sim.Beam.xangle or ConfigFlags.Sim.Beam.yangle or
                  ConfigFlags.Sim.Beam.xshift or ConfigFlags.Sim.Beam.yshift)

#     from math import atan
#     from AthenaCommon.SystemOfUnits import GeV, TeV, cm, m
#     from AthenaCommon.PhysicalConstants import pi
#     import ParticleGun as PG
#     ConfigFlags.Sim.Gun = {"Generator" : "SingleParticle", "pid" : 11, "energy" : PG.LogSampler(10*GeV, 1*TeV), "theta" :
#                            PG.GaussianSampler(0, atan((10*cm)/(7*m)), oneside = True), "phi" : [0, 2*pi], "mass" : 0.511, "radius" : -10*cm, "randomSeed" : 12345}

    if doShiftLOS:
        pgConfig = ConfigFlags.Sim.Gun
        pgConfig["McEventKey"] = "BeamTruthEvent_ATLASCoord"
        ConfigFlags.Sim.Gun = pgConfig


#
# By being a little clever, we can steer the geometry setup from the command line using GeoModel.FaserVersion
#
# Start with minimal configuration for Testbeam
#
    detectors = ['Veto', 'Preshower', 'FaserSCT', 'Ecal']
    if ConfigFlags.GeoModel.FaserVersion.count("TB") == 0 :
        detectors += ['Trigger', 'Dipole']
    if ConfigFlags.GeoModel.FaserVersion.count("FASERNU") > 0 :
        detectors += ['Emulsion']
    if ConfigFlags.GeoModel.FaserVersion.count("FASERNU-03") > 0 or ConfigFlags.GeoModel.FaserVersion.count("FASERNU-04") > 0:
        detectors += ['VetoNu', 'Trench', 'Muon']
#
# Setup detector flags
#
    from CalypsoConfiguration.DetectorConfigFlags import setupDetectorsFromList
    setupDetectorsFromList(ConfigFlags, detectors, toggle_geometry=True)
#
# Finalize flags
#
    ConfigFlags.lock()
#
# Initialize a new component accumulator
#
    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    cfg = MainServicesCfg(ConfigFlags)
#
# Check whether a real input file was specified
#
    if ConfigFlags.Input.Files and ConfigFlags.Input.Files != ["_CALYPSO_GENERIC_INPUTFILE_NAME_"] :
        print("Input.Files = ",ConfigFlags.Input.Files)

#
# If so, and only one file that ends in .events or .hepmc read as HepMC
#
        if len(ConfigFlags.Input.Files) == 1 and (ConfigFlags.Input.Files[0].endswith(".events") or ConfigFlags.Input.Files[0].endswith(".hepmc")):

            from HEPMCReader.HepMCReaderConfig import HepMCReaderCfg

            if doShiftLOS:
                cfg.merge(HepMCReaderCfg(ConfigFlags, McEventKey = "BeamTruthEvent_ATLASCoord"))
            else:
                cfg.merge(HepMCReaderCfg(ConfigFlags))

            from McEventSelector.McEventSelectorConfig import McEventSelectorCfg
            cfg.merge(McEventSelectorCfg(ConfigFlags))

#
# Else, set up to read it as a pool.root file
#
        else:
            from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
            cfg.merge(PoolReadCfg(ConfigFlags))

            if doShiftLOS:
                from SGComps.AddressRemappingConfig import InputOverwriteCfg
                # Rename old truth collection to add ATLAS coord to can still use BeamTruthEvent for the one in FASER Coords
                cfg.merge(InputOverwriteCfg("McEventCollection", "BeamTruthEvent", "McEventCollection", "BeamTruthEvent_ATLASCoord"))

#
# If not, configure the particle gun as requested, or using defaults
#
    else :
#
# Particle gun generators - the generator, energy, angle, particle type, position, etc can be modified by passing keyword arguments
#
        from FaserParticleGun.FaserParticleGunConfig import FaserParticleGunCfg
        cfg.merge(FaserParticleGunCfg(ConfigFlags))
        
        from McEventSelector.McEventSelectorConfig import McEventSelectorCfg
        cfg.merge(McEventSelectorCfg(ConfigFlags))

#
# Output file
#
    from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
    cfg.merge(PoolWriteCfg(ConfigFlags))

#
# Shift LOS
#

    if doShiftLOS:

        import McParticleEvent.Pythonizations
        from GeneratorUtils.ShiftLOSConfig import ShiftLOSCfg
        cfg.merge(ShiftLOSCfg(ConfigFlags, 
                              xcross = ConfigFlags.Sim.Beam.xangle, ycross = ConfigFlags.Sim.Beam.yangle,
                              xshift = ConfigFlags.Sim.Beam.xshift, yshift = ConfigFlags.Sim.Beam.yshift))
    
#
# Add the G4FaserAlg
#
    from G4FaserAlg.G4FaserAlgConfigNew import G4FaserAlgCfg
    cfg.merge(G4FaserAlgCfg(ConfigFlags))

    #cfg.getEventAlgo("OutputStreamHITS").ItemList += ["McEventCollection#BeamTruthEvent_ATLASCoord"]    
#
# Uncomment to check volumes for overlap - will cause CTest to fail due to overwriting file
#
#    from G4DebuggingTools.G4DebuggingToolsConfigNew import VolumeDebugger
#    cfg.merge(VolumeDebugger(ConfigFlags, name="G4UA::UserActionSvc", TargetVolume="", Verbose=True))
#
# Dump config
#
    from AthenaConfiguration.ComponentFactory import CompFactory
    cfg.addEventAlgo(CompFactory.JobOptsDumperAlg(FileName="G4FaserTestConfig.txt"))
    cfg.getService("StoreGateSvc").Dump = True
    cfg.getService("ConditionStore").Dump = True
    cfg.printConfig(withDetails=True, summariseProps = False)  # gags on ParticleGun if summariseProps = True?

    ConfigFlags.dump()
    f = open("test.pkl","wb")
    cfg.store(f)
    f.close()
#
# Execute and finish
#
    #cfg.foreach_component("*").OutputLevel = VERBOSE

    sc = cfg.run()

    b = time.time()
    log.info("Run G4FaserAlg in " + str(b-a) + " seconds")
#
# Success should be 0
#
    sys.exit(not sc.isSuccess())

