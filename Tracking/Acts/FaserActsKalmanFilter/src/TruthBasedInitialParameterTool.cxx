#include "TruthBasedInitialParameterTool.h"

#include "StoreGate/ReadHandle.h"
#include "HepMC/GenVertex.h"
#include "Acts/EventData/TrackParameters.hpp"
#include "Acts/Definitions/Units.hpp"
#include <random>
#include <cmath>


using namespace Acts::UnitLiterals;

TruthBasedInitialParameterTool::TruthBasedInitialParameterTool(
    const std::string& type, const std::string& name, const IInterface* parent) :
    AthAlgTool(type, name, parent) {}


StatusCode TruthBasedInitialParameterTool::initialize() {
  ATH_MSG_INFO("TruthBasedInitialParameterTool::initialize");
  ATH_CHECK(m_simDataCollectionKey.initialize());
  ATH_CHECK(m_simWriterTool.retrieve());
  return StatusCode::SUCCESS;
}


StatusCode TruthBasedInitialParameterTool::finalize() {
  ATH_MSG_INFO("TruthBasedInitialParameterTool::finalize");
  return StatusCode::SUCCESS;
}


Acts::CurvilinearTrackParameters TruthBasedInitialParameterTool::getInitialParameters(std::vector<Identifier> ids) const {
  SG::ReadHandle<TrackerSimDataCollection> simDataCollection(m_simDataCollectionKey);

  // get simulated vertex and momentum of the HepMcParticle with the highest energy deposit
  float highestDep = 0;
  HepMC::FourVector vertex;
  HepMC::FourVector momentum;

  for (Identifier id : ids) {
    if( simDataCollection->count(id) == 0)
      continue;

    const auto& deposits = simDataCollection->at(id).getdeposits();
    for( const auto& depositPair : deposits) {
      if( std::abs(depositPair.first->pdg_id()) == 13 && depositPair.second > highestDep) {
        highestDep = depositPair.second;
        vertex = depositPair.first->production_vertex()->position();
        momentum = depositPair.first->momentum();
      }
    }
  }

  Acts::Vector3 truthVertex = {vertex.x(), vertex.y(), vertex.z()}; // in mm
  Acts::Vector3 truthMomentum = {momentum.x() / 1000, momentum.y() / 1000, momentum.z() / 1000}; // in GeV
  m_simWriterTool->writeout(truthVertex, truthMomentum);

  // smearing parameters
  double sigmaU = 200_um;
  double sigmaV = 200_um;
  double sigmaPhi = 1_degree;
  double sigmaTheta = 1_degree;
  double sigmaQOverP = 0.1 * truthVertex.norm() / (truthMomentum.norm() * truthMomentum.norm());
  double sigmaT0 = 1_ns;

  // create covariance matrix
  Acts::BoundSymMatrix cov = Acts::BoundSymMatrix::Zero();
  cov(Acts::eBoundLoc0, Acts::eBoundLoc1) = sigmaU * sigmaU;
  cov(Acts::eBoundLoc1, Acts::eBoundLoc1) = sigmaV * sigmaV;
  cov(Acts::eBoundPhi, Acts::eBoundPhi) = sigmaPhi * sigmaPhi;
  cov(Acts::eBoundTheta, Acts::eBoundTheta) = sigmaTheta * sigmaTheta;
  cov(Acts::eBoundQOverP, Acts::eBoundQOverP) = sigmaQOverP * sigmaQOverP;
  cov(Acts::eBoundTime, Acts::eBoundTime) = sigmaT0 * sigmaT0;
   
  // smear truth parameters
  std::random_device rd;
  std::default_random_engine rng {rd()};
  std::normal_distribution<> norm; // mu: 0 sigma: 1
  double charge = 1;
  double time =0;
  Acts::Vector3 deltaPos(sigmaU*norm(rng), sigmaU*norm(rng), sigmaU*norm(rng));
  Acts::Vector4 posTime = {(truthVertex+deltaPos).x(), (truthVertex+deltaPos).y(), (truthVertex+deltaPos).z(), time};
  auto theta = Acts::VectorHelpers::theta(truthMomentum.normalized());
  auto phi = Acts::VectorHelpers::phi(truthMomentum.normalized());
  auto angles = Acts::detail::normalizePhiTheta(phi + sigmaPhi*norm(rng), theta + sigmaTheta*norm(rng));
  Acts::Vector3 dir(std::sin(angles.second) * std::cos(angles.first),
                    std::sin(angles.second) * std::sin(angles.first),
                    std::cos(angles.second));
  const Acts::Vector3 initMomentum = ( truthMomentum.norm()*(1 + 0.1*norm(rng)) ) * dir;
  const Acts::Vector3 momentum_dir = initMomentum.normalized();
  double momentum_abs = initMomentum.norm();

  ATH_MSG_DEBUG("x: " << posTime.x() << ", y: " << posTime.y() << ", z: " << posTime.z());
  ATH_MSG_DEBUG("p: " << momentum_abs << ", px: " << momentum_dir.x() << ", py: " << momentum_dir.y() << ", pz: " << momentum_dir.z());
  
  Acts::CurvilinearTrackParameters initialTrackParameters(posTime, momentum_dir, momentum_abs, charge, std::make_optional(std::move(cov)));
  return initialTrackParameters;
}