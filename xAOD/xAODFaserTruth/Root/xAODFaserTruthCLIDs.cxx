/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


//simple includes to force the CLASS_DEF to be encountered during compile

#include "xAODFaserTruth/FaserTruthVertexContainer.h"
#include "xAODFaserTruth/FaserTruthVertexAuxContainer.h"
#include "xAODFaserTruth/FaserTruthEventContainer.h"
#include "xAODFaserTruth/FaserTruthEventAuxContainer.h"
#include "xAODFaserTruth/FaserTruthParticleContainer.h"
#include "xAODFaserTruth/FaserTruthParticleAuxContainer.h"

