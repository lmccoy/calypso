/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// Local include(s):
#include "xAODFaserTracking/StripClusterAuxContainer.h"
 
namespace xAOD {
 
  StripClusterAuxContainer::StripClusterAuxContainer()
    : AuxContainerBase() {
      AUX_VARIABLE( id );
      AUX_VARIABLE( rdoIdentifierList );

      AUX_VARIABLE( localX );
      AUX_VARIABLE( localY );
      AUX_VARIABLE( localXError );
      AUX_VARIABLE( localYError );
      AUX_VARIABLE( localXYCorrelation );

      AUX_VARIABLE( globalX );
      AUX_VARIABLE( globalY );
      AUX_VARIABLE( globalZ );
  }
  
  void StripClusterAuxContainer::dump() const {
    std::cout<<" Dumping StripClusterAuxContainer"<<std::endl;
    std::cout<<"id:";
    std::copy(id.begin(), id.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"localX:";
    std::copy(localX.begin(), localX.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"localY:";
    std::copy(localY.begin(), localY.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"localXError:";
    std::copy(localXError.begin(), localXError.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"localYError:";
    std::copy(localYError.begin(), localYError.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"localXYCorrelation:";
    std::copy(localXYCorrelation.begin(), localXYCorrelation.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"globalX:";
    std::copy(globalX.begin(), globalX.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"globalY:";
    std::copy(globalY.begin(), globalY.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"globalZ:";
    std::copy(globalZ.begin(), globalZ.end(),
        std::ostream_iterator<float>(std::cout, ", "));
        std::cout<<std::endl;
  }

} // namespace xAOD