#ifndef XAODFASERTRACKING_TRACKCONTAINER_H
#define XAODFASERTRACKING_TRACKCONTAINER_H

// Core include(s):
#include "AthContainers/DataVector.h"
#include <stdint.h>
 
// Local include(s):
#include "xAODFaserTracking/Track.h"

//template struct DataVector_detail::DVLEltBaseInit< xAOD::Track >;
 
namespace xAOD {
   typedef DataVector< xAOD::Track > TrackContainer;
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackContainer, 1287447991, 1 )

#endif // XAODFASERTRACKING_TRACKCONTAINER_H