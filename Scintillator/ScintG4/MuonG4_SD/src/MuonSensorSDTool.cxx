/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// Muon Sensitive Detector Tool.
//

// class header
#include "MuonSensorSDTool.h"

// package includes
#include "MuonSensorSD.h"

// STL includes
#include <exception>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MuonSensorSDTool::MuonSensorSDTool(const std::string& type, const std::string& name, const IInterface* parent)
  : SensitiveDetectorBase( type , name , parent )
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VSensitiveDetector* MuonSensorSDTool::makeSD() const
{
  ATH_MSG_DEBUG( "Creating Muon SD: " << name() );
  return new MuonSensorSD(name(), m_outputCollectionNames[0]);
}

