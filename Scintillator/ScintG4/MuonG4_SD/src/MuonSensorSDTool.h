/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/****************************************************************
   Muon Sensitive Detector Tool
 ****************************************************************/

#ifndef MUONG4_SD_MUONSENSORSDTOOL_H
#define MUONG4_SD_MUONSENSORSDTOOL_H

// Base class
#include "G4AtlasTools/SensitiveDetectorBase.h"

// STL headers
#include <string>

class G4VSensitiveDetector;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

class MuonSensorSDTool : public SensitiveDetectorBase
{
 public:
  // Constructor
  MuonSensorSDTool(const std::string& type, const std::string& name, const IInterface *parent);

  // Destructor
  ~MuonSensorSDTool() { /* If all goes well we do not own myHitColl here */ }

protected:
  // Make me an SD!
  G4VSensitiveDetector* makeSD() const override final;
};

#endif //MUONG4_SD_MUONSENSORSDTOOL_H
