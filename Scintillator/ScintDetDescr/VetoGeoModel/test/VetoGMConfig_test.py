#!/usr/bin/env python
"""Run tests on VetoGeoModel configuration

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior=1
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    from AthenaConfiguration.TestDefaults import defaultTestFiles

    ConfigFlags.Input.Files = defaultTestFiles.HITS
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"             # Always needed; must match FaserVersion
    ConfigFlags.GeoModel.Align.Dynamic    = False
    ConfigFlags.lock()

    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    from VetoGeoModel.VetoGeoModelConfig import VetoGeometryCfg
    acc = VetoGeometryCfg(ConfigFlags)
    f=open('VetoGeometryCfg.pkl','wb')
    acc.store(f)
    f.close()
