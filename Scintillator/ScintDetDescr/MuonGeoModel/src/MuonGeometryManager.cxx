/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonGeometryManager.h"

#include "ScintGeoModelUtils/DistortedMaterialManager.h"
#include "ScintIdentifier/MuonID.h"
#include "ScintReadoutGeometry/ScintCommonItems.h"
#include "MuonGeoModel/MuonGeoModelAthenaComps.h"

MuonGeometryManager::MuonGeometryManager(const MuonGeoModelAthenaComps* athenaComps)
  : m_athenaComps{athenaComps}
{
  // This class uses reference counting. Should not be delete'd in destructor.
  m_commonItems = new ScintDD::ScintCommonItems(m_athenaComps->getIdHelper());
}

MuonGeometryManager::~MuonGeometryManager()
{
}

//
// Access to run time options.
//
const MuonOptions & 
MuonGeometryManager::options() const
{
  return m_options;
}

void
MuonGeometryManager::setOptions(const MuonOptions & options)
{
  m_options = options;
}

const MuonGeoModelAthenaComps *
MuonGeometryManager::athenaComps() const 
{
  return m_athenaComps;
}
  
//
// ScintCommonItems which are passed to ScintDetectorElements.
//

const ScintDD::ScintCommonItems *
MuonGeometryManager::commonItems() const
{
  return m_commonItems;
}

MuonGeometryManager&
MuonGeometryManager::operator=(const MuonGeometryManager& right) {
  if (this != &right) {
    m_options = right.m_options;
    m_athenaComps = right.m_athenaComps;
    m_commonItems = right.m_commonItems;
  }
  return *this;
}

MuonGeometryManager::MuonGeometryManager(const MuonGeometryManager& right) {
  m_options = right.m_options;
  m_athenaComps = right.m_athenaComps;
  m_commonItems = right.m_commonItems;
}
