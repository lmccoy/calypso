/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonGeoModel/MuonGeoModelAthenaComps.h"

MuonGeoModelAthenaComps::MuonGeoModelAthenaComps()
  : ScintDD::AthenaComps("MuonGeoModel"),
    m_idHelper(0)
{}
 
void 
MuonGeoModelAthenaComps::setIdHelper(const MuonID* idHelper)
{
  m_idHelper = idHelper;
}

const MuonID* 
MuonGeoModelAthenaComps::getIdHelper() const
{
  return m_idHelper;
}

