#include "MuonDetectorFactory.h"
#include "MuonOptions.h"
#include "MuonGeoModel/MuonGeoModelAthenaComps.h"

#include "GeoModelXMLParser/XercesParser.h"
#include "PathResolver/PathResolver.h"

#include "ScintReadoutGeometry/ScintDetectorDesign.h"


#include "GeoModelKernel/GeoLogVol.h"  
#include "GeoModelKernel/GeoNameTag.h"  
#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoFullPhysVol.h"
#include "GeoModelKernel/GeoTransform.h"  
#include "GeoModelKernel/GeoAlignableTransform.h" 
#include "GeoModelKernel/GeoBox.h"
#include "StoreGate/StoreGateSvc.h"
#include "GeoModelFaserUtilities/DecodeFaserVersionKey.h"


#include "RDBAccessSvc/IRDBRecord.h"
#include "RDBAccessSvc/IRDBRecordset.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"

#include "GaudiKernel/MsgStream.h"

#include "AthenaKernel/ClassID_traits.h"
#include "SGTools/DataProxy.h"

//test
#include "GeoModelKernel/GeoIdentifierTag.h"  

#include <string>
#include <map>
#include <iostream> //Temp for debugging

#include "MuonGeoModel/MuonDetectorTool.h"
#include "MuonDetectorFactory.h" 
#include "MuonOptions.h"
#include "ScintReadoutGeometry/MuonDetectorManager.h" 
#include "ScintReadoutGeometry/ScintDetectorElement.h"
#include "ScintIdentifier/MuonID.h"

#include "GeoModelInterfaces/IGeoDbTagSvc.h"
#include "GeoModelFaserUtilities/GeoModelExperiment.h"
#include "GeoModelFaserUtilities/DecodeFaserVersionKey.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"
#include "RDBAccessSvc/IRDBRecord.h"
#include "RDBAccessSvc/IRDBRecordset.h"
#include "StoreGate/DataHandle.h"


MuonDetectorFactory::MuonDetectorFactory(const MuonGeoModelAthenaComps * athenaComps, 
		                                 const MuonOptions & options)
  : ScintDD::DetectorFactoryBase(athenaComps),
    m_useDynamicAlignFolders(false)
{

  // Create the detector manager
  m_detectorManager = new ScintDD::MuonDetectorManager(detStore());
  msg(MSG::DEBUG) << "Created MuonDetectorManager" << endmsg;

//   // Create the database
//   m_db = new MuonDataBase{athenaComps};
//   msg(MSG::DEBUG) << "Created MuonDataBase" << endmsg;

//   // Create the material manager
//   m_materials = new MuonMaterialManager{m_db};
//   msg(MSG::DEBUG) << "Created MuonMaterialManager" << endmsg;

  // Create the geometry manager.
  m_geometryManager = new MuonGeometryManager(athenaComps);
  msg(MSG::DEBUG) << "Created MuonGeometryManager" << endmsg;
  m_geometryManager->setOptions(options);
  msg(MSG::DEBUG) << "Set options on MuonGeometryManager" << endmsg;

  m_useDynamicAlignFolders = options.dynamicAlignFolders();
 
  // Set Version information
  // Get the geometry tag
  DecodeFaserVersionKey versionKey(geoDbTagSvc(),"Muon");
  IRDBRecordset_ptr switchSet
    = rdbAccessSvc()->getRecordsetPtr("MuonSwitches", versionKey.tag(), versionKey.node(),"FASERDD");
  const IRDBRecord    *switches   = (*switchSet)[0];
  msg(MSG::DEBUG) << "Retrieved MuonSwitches" << endmsg;

  std::string layout = "Final";
  std::string description;
  if (!switches->isFieldNull("LAYOUT")) {
    layout = switches->getString("LAYOUT");
  }
  if (!switches->isFieldNull("DESCRIPTION")) {
    description = switches->getString("DESCRIPTION");
  }

  std::string versionTag = rdbAccessSvc()->getChildTag("Muon", versionKey.tag(), versionKey.node(),"FASERDD");
  std::string versionName = switches->getString("VERSIONNAME");
  int versionMajorNumber = 1;
  int versionMinorNumber = 0;
  int versionPatchNumber = 0;
  ScintDD::Version version(versionTag,
                           versionName, 
                           layout, 
                           description, 
                           versionMajorNumber,
                           versionMinorNumber,
                           versionPatchNumber);
  m_detectorManager->setVersion(version);


    // //Create the manager for the detector, for accessing the geometry
    // m_detectorManager = new ScintDD::MuonDetectorManager(detStore());
    // msg(MSG::DEBUG) << "Created MuonDetectorManager" << endmsg;

    std::string gdmlFile; //A string to store the name of the gdml file from the database

    msg(MSG::DEBUG) << "Getting services" <<  endmsg;

    // //Use the IRDBRecordSvc to retrieve the gdml file from the database
    // //Start by getting a IRDVRecordset from the service
    // IRDBRecordset_ptr switchSet{m_access->getRecordsetPtr("MuonSwitches", m_versionTag, m_versionNode, "FASERDD")};
    // //check to make sure the recordset is not null or empty
    // if (!switchSet || switchSet->size() == 0)
    // {
    //     //recordset is either null or empty display warning message
    //     MsgStream gLog(Athena::getMessageSvc(), "MuonDetectorFactory");
    //     gLog << MSG::WARNING << "Unable to retrieve switches; Muon Detector cannot be created" << endmsg;
    //     return; 
    // }

    msg(MSG::DEBUG) << "Getting GDML File" << endmsg;

    //use the record set to get the specific record of interest
    // const IRDBRecord* switches{(*switchSet)[0]};
    //if check the gdml file name field, if it is not empty they put it's value into gdmlFile, otherwise display warning message
    if (!switches->isFieldNull("GDMLFILE") && gdmlFile.empty())
    {
        gdmlFile = switches->getString("GDMLFILE");
    }
    else if (gdmlFile.empty())
    {
        MsgStream gLog(Athena::getMessageSvc(), "MuonDetectoryFactory");
        gLog << MSG::WARNING << "GDML file name not found; Muon detector cannot be created" << endmsg;
        return;
    }

    msg(MSG::DEBUG) << "Resolving path" <<  endmsg;

    //right now gdmlFile is the name of just the file, not the absolute path name, in order to parse the gdml
    //file into a geometry tree we need the absolute path name. PathResolver::find_file is a function that will
    //search the local machine for the inputted file, and return the absolute path filename. Store the absolute
    //name into a variable called resolvedFile
    m_resolvedFile = PathResolver::find_file(gdmlFile, "XMLPATH", PathResolver::RecursiveSearch);
    msg(MSG::DEBUG) << "Resolved path: " << m_resolvedFile <<  endmsg;

}

MuonDetectorFactory::~MuonDetectorFactory()
{
}

void MuonDetectorFactory::create(GeoPhysVol *world)
{
    auto store = XMLHandlerStore::GetHandlerStore();
    store->clear();

    GDMLController controller {"MuonGDMLController"};

    XercesParser xercesParser; //A parser that will parser our gdml file into a geometery

    msg(MSG::DEBUG) << "Parsing GDML File" <<  endmsg;

    //parse the gdml file, which reads the gdml file and stores it as an XMLString
    //Then nagivate the XMLString and to create nodes for the GDMLController
    xercesParser.ParseFileAndNavigate(m_resolvedFile);

    msg(MSG::DEBUG) << "Using parsed file to create geometry" <<  endmsg;

    //Once the gdml file is read and parsed it may be used to created the geometry for the detector
    GeoTrf::Translate3D muonTranslation(0, 0, 4500);
    GeoAlignableTransform* muonTransform = new GeoAlignableTransform(muonTranslation);
    GeoPhysVol* muonPhysicalVolume = controller.retrieveLogicalVolume("Muon").second; //first get the top physical volume from the controller
    GeoNameTag* tag = new GeoNameTag("Muon"); //create a tag for the muon detecotor
    world->add(tag); //add the tag to the world's volume
    world->add(new GeoIdentifierTag(0));
    world->add(muonTransform);
    world->add(muonPhysicalVolume); //add the muon detector's volume to the world's volume
    m_detectorManager->addTreeTop(muonPhysicalVolume); //add the muon detector's volume to the muon manager

    //Create and add detector elements

    //Note all plates have material of scintillatorMaterial, will use this to identify them
    const std::string scintMaterial = "scintillatorMaterial";
    const MuonID* helper = m_geometryManager->athenaComps()->getIdHelper();
    uint plateIndex = 0;
    for (uint childVolumeIndex = 0; childVolumeIndex < muonPhysicalVolume->getNChildVols(); childVolumeIndex++)
    {
      //muonPhysicalVolume->getChildVol give a smart pointer to the volume at given index
      msg(MSG::DEBUG) << "MuonVolumeChildrenName: " << muonPhysicalVolume->getChildVol(childVolumeIndex)->getLogVol()->getMaterial()->getName() << endmsg;

      //check if this volume is a plate
      if (scintMaterial != muonPhysicalVolume->getChildVol(childVolumeIndex)->getLogVol()->getMaterial()->getName())
      {
        //not a plate continuing
        continue;
      }

      //Create identifiers for each plate
      Identifier id = helper->plate_id(0, plateIndex++);

      //Create detector design
      const GeoBox* plateBox = dynamic_cast<const GeoBox*>(muonPhysicalVolume->getChildVol(childVolumeIndex)->getLogVol()->getShape());
      if (plateBox == nullptr)
      {
        msg(MSG::FATAL) << "Muon plate is an incorrect shape" << endmsg;
        return;
      }
      //Use GeoBox to get each half length and double, All muon plates have 1 pmt (for last argument)
      ScintDD::ScintDetectorDesign* plateDesign = new ScintDD::ScintDetectorDesign(2*plateBox->getXHalfLength(), 2*plateBox->getYHalfLength(), 2*plateBox->getZHalfLength(), 1);

      //Retrieve element's volume from muonPhysicalVolume for each element
      GeoFullPhysVol* plateFullPhysicalVolume = new GeoFullPhysVol(muonPhysicalVolume->getChildVol(childVolumeIndex)->getLogVol());

      //Get the common items from the geoManager
      const ScintDD::ScintCommonItems* muonCommonItems = m_geometryManager->commonItems();


      //Create detector element and add to the manager for each element
      ScintDD::ScintDetectorElement* plate = new ScintDD::ScintDetectorElement(id, plateDesign, plateFullPhysicalVolume, muonCommonItems);
      m_detectorManager->addDetectorElement(plate);
      msg(MSG::DEBUG) << "Created MuonDetectorElement " << plateIndex-1 << endmsg;


    }

}

const ScintDD::MuonDetectorManager * MuonDetectorFactory::getDetectorManager() const
{
    return m_detectorManager;
}

void MuonDetectorFactory::setTagNode(const std::string& tag, const std::string& node)
{
    m_versionTag = tag;
    m_versionNode = node;
}