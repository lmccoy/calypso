#ifndef MuonDetectorFactory_h
#define MuonDetectorFactory_h 1

// #include "GeoModelKernel/GeoVDetectorFactory.h"
#include "ScintGeoModelUtils/DetectorFactoryBase.h" 
#include "ScintReadoutGeometry/ScintDD_Defs.h"
#include "ScintReadoutGeometry/MuonDetectorManager.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"
#include "GDMLInterface/GDMLController.h"
#include "MuonGeometryManager.h"

#include <string>

class StoreGateSvc;
class MuonGeoModelAthenaComps;
class MuonOptions;

class MuonDetectorFactory : public ScintDD::DetectorFactoryBase 
{
    public:

    // Constructor
    MuonDetectorFactory(const MuonGeoModelAthenaComps * athenaComps, 
		                const MuonOptions & options);

    // Destructor
    ~MuonDetectorFactory();

    // Creation of geometry
    virtual void create(GeoPhysVol *world);

    // Asscess to the results
    virtual const ScintDD::MuonDetectorManager *getDetectorManager() const;

    // Set version Tag and Node
    void setTagNode(const std::string& tag, const std::string& node);

private:
    // Illegal operations
    const MuonDetectorFactory & operator=(const MuonDetectorFactory &right);
    MuonDetectorFactory(const MuonDetectorFactory &right);

    // The manager
    ScintDD::MuonDetectorManager *m_detectorManager;

    StoreGateSvc *m_detectorStore;
    IRDBAccessSvc *m_access;
    std::string m_versionTag;
    std::string m_versionNode;
    bool m_useDynamicAlignFolders;
    std::string m_resolvedFile;

    MuonGeometryManager* m_geometryManager;
};

#endif