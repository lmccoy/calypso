#include "MuonGeoModel/MuonDetectorTool.h"
#include "MuonDetectorFactory.h" 
#include "MuonOptions.h"
#include "ScintReadoutGeometry/MuonDetectorManager.h" 
#include "ScintIdentifier/MuonID.h"

#include "GeoModelInterfaces/IGeoDbTagSvc.h"
#include "GeoModelFaserUtilities/GeoModelExperiment.h"
#include "GeoModelFaserUtilities/DecodeFaserVersionKey.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"
#include "RDBAccessSvc/IRDBRecord.h"
#include "RDBAccessSvc/IRDBRecordset.h"
#include "StoreGate/DataHandle.h"

#include "AthenaKernel/ClassID_traits.h"
#include "SGTools/DataProxy.h"

#include "boost/algorithm/string/predicate.hpp"

MuonDetectorTool::MuonDetectorTool(const std::string& type, const std::string& name, const IInterface* parent)
                 : GeoModelTool(type, name, parent),
                    m_manager{nullptr},
                    m_athenaComps{ },
                    m_geoDbTagSvc{"GeoDbTagSvc", name},
                    m_rdbAccessSvc{"RDBAccessSvc", name},
                    m_geometryDBSvc{"ScintGeometryDBSvc", name}
{
  // Get parameter values from jobOptions file
  declareProperty("GeoDbTagSvc", m_geoDbTagSvc);
  declareProperty("RDBAccessSvc", m_rdbAccessSvc);
  declareProperty("GeometryDBSvc", m_geometryDBSvc);
}

MuonDetectorTool::~MuonDetectorTool()
{
}

StatusCode MuonDetectorTool::create()
{
  // Get the detector configuration.
  ATH_CHECK(m_geoDbTagSvc.retrieve());
  
  DecodeFaserVersionKey versionKey{&*m_geoDbTagSvc, "Muon"};
  // Issue error if AUTO.
  if (versionKey.tag() == "AUTO") {
    ATH_MSG_ERROR("AUTO Faser version. Please select a version.");
  }
  ATH_MSG_INFO("Building Muon with Version Tag: " << versionKey.tag() << " at Node: " << versionKey.node());

  ATH_CHECK(m_rdbAccessSvc.retrieve());
  // Print the Muon version tag:
  std::string muonVersionTag{m_rdbAccessSvc->getChildTag("Muon", versionKey.tag(), versionKey.node(), "FASERDD")};
  ATH_MSG_INFO("Muon Version: " << muonVersionTag);
  // Check if version is empty. If so, then the Muon cannot be built. This may or may not be intentional. We
  // just issue an INFO message. 
  if (muonVersionTag.empty()) {
    ATH_MSG_INFO("No Muon Version. Muon will not be built.");
  } else {
    std::string versionName;
    if (versionKey.custom()) {
      ATH_MSG_WARNING("MuonDetectorTool:  Detector Information coming from a custom configuration!!");
    } else {
      ATH_MSG_DEBUG("MuonDetectorTool:  Detector Information coming from the database and job options IGNORED.");
      ATH_MSG_DEBUG("Keys for Muon Switches are "  << versionKey.tag()  << "  " << versionKey.node());

      IRDBRecordset_ptr switchSet{m_rdbAccessSvc->getRecordsetPtr("MuonSwitches", versionKey.tag(), versionKey.node(), "FASERDD")};
      const IRDBRecord* switches{(*switchSet)[0]};
      m_detectorName.setValue(switches->getString("DETECTORNAME"));

      if (not switches->isFieldNull("VERSIONNAME")) 
      {
        versionName = switches->getString("VERSIONNAME");
      } 
    }

    ATH_MSG_DEBUG("Creating the Muon detector");
    ATH_MSG_DEBUG("Muon Geometry Options: ");
    ATH_MSG_DEBUG(" Alignable:             " << (m_alignable.value() ? "true" : "false"));
    ATH_MSG_DEBUG(" VersionName:           " << versionName);

    MuonOptions options;
    options.setAlignable(m_alignable.value());
    options.setDynamicAlignFolders(m_useDynamicAlignFolders.value());

    //get the database for the the Faser experiment
    GeoModelExperiment* faserExperiment = nullptr;
    StatusCode statusCode = detStore()->retrieve(faserExperiment, "FASER");
    if (statusCode.isFailure())
    {
        //could not retrieve the geomodel for the Faser experiment, display error message
        msg(MSG::ERROR) << "Could not find GeoModelExperiment FASER" << endmsg;
        return statusCode;
    }

    //get the world volume from the experiment
    GeoPhysVol* world = faserExperiment->getPhysVol();

    // Pass athena services to factory, etc
    m_athenaComps.setDetStore(detStore().operator->());
    m_athenaComps.setGeoDbTagSvc(&*m_geoDbTagSvc);
    m_athenaComps.setGeometryDBSvc(&*m_geometryDBSvc);
    m_athenaComps.setRDBAccessSvc(&*m_rdbAccessSvc);
    const MuonID* idHelper{nullptr};
    ATH_CHECK(detStore()->retrieve(idHelper, "MuonID"));
    m_athenaComps.setIdHelper(idHelper);
    idHelper->test_plate_packing();

    //Check if the geometry has been built already
    if (!m_manager)
    {
        //geometry is not build, use the factory to create it
        MuonDetectorFactory muonDetectorFactory(&m_athenaComps, options);
        muonDetectorFactory.setTagNode(versionKey.tag(), versionKey.node());
        muonDetectorFactory.create(world);
        m_manager = muonDetectorFactory.getDetectorManager();
    }

    //if the factory still fail to create the geometry then the manager could still be nullptr, have to check
    if (m_manager)
    {
        //add the muon detector manager to the faser experiment
        faserExperiment->addManager(m_manager);
        statusCode = detStore()->record(m_manager, m_manager->getName());
        if (statusCode.isFailure())
        {
            msg(MSG::ERROR) << "Could not register muon detector manager" << endmsg;
            return statusCode;
        }
    }
    else
    {
        //in the event the muon detector geometry could not be created the tool failed, display error message
        msg(MSG::ERROR) << "ERROR. Failed to build Muon Detector Version " << muonVersionTag << endmsg;
        return StatusCode::FAILURE;
    }

    //If the function has not returned yet then the tool successfully did its job
  }
  return StatusCode::SUCCESS;
}

StatusCode MuonDetectorTool::clear()
{
    SG::DataProxy* proxy = detStore()->proxy(ClassID_traits<ScintDD::MuonDetectorManager>::ID(), m_manager->getName());
    if (proxy)
    {
        proxy->reset();
        m_manager = nullptr;
    }

    return StatusCode::SUCCESS;
}