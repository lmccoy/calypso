/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef MuonGeoModel_MuonGeometryManager_H
#define MuonGeoModel_MuonGeometryManager_H

#include "MuonOptions.h"

#include <memory>

namespace ScintDD {
  class ScintCommonItems;
}

class MuonStationParameters;
class MuonPlateParameters;
class MuonRadiatorParameters;
class MuonAbsorberParameters;
class MuonDataBase;
class MuonGeneralParameters;
class MuonGeoModelAthenaComps;

//There are many representations for similar or identical information
//such as athenaComps vs commonItems.
//The purpose of this class will be to serve as container for all these
//different representations, so they can be keep in one place. It is a
//trimmed down version of Preshower's Geometry Manager, since Muon has
//less softwares parts than Preshower does
class MuonGeometryManager {

public:

  // Constructor 
  MuonGeometryManager(const MuonGeoModelAthenaComps* athenaComps);

  // Destructor 
  ~MuonGeometryManager();

  // Access to run time options
  const MuonOptions & options() const;
  void setOptions(const MuonOptions & options);

  // Access to athena components
  const MuonGeoModelAthenaComps * athenaComps() const;

  // To be passed to detector element.
  const ScintDD::ScintCommonItems * commonItems() const;

  MuonGeometryManager& operator=(const MuonGeometryManager& right);
  MuonGeometryManager(const MuonGeometryManager& right);

private:

  MuonOptions m_options;
  const MuonGeoModelAthenaComps * m_athenaComps;
  ScintDD::ScintCommonItems * m_commonItems;
};


#endif // MuonGeoModel_MuonGeometryManager_H
