/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef MuonGeoModel_MuonGeoModelAthenaComps_H
#define MuonGeoModel_MuonGeoModelAthenaComps_H 1

#include "ScintGeoModelUtils/ScintDDAthenaComps.h"

class MuonID;

/// Class to hold various Athena components
// template <class ID_HELPER>
class MuonGeoModelAthenaComps : public ScintDD::AthenaComps {

public:

  MuonGeoModelAthenaComps();

  void setIdHelper(const MuonID* idHelper);

  const MuonID* getIdHelper() const;

private:
  const MuonID* m_idHelper;

};

#endif // MuonGeoModel_MuonGeoModelAthenaComps_H
