#ifndef MUONDETECTORTOOL_H
#define MUONDETECTORTOOL_H

#include "GeoModelFaserUtilities/GeoModelTool.h"
#include "MuonGeoModel/MuonGeoModelAthenaComps.h"

#include "GeometryDBSvc/IGeometryDBSvc.h"
#include "GeoModelInterfaces/IGeoDbTagSvc.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"

#include "GaudiKernel/ServiceHandle.h"

// #include <string>

namespace ScintDD
{
    class MuonDetectorManager;    
} // namespace ScintDD

class MuonDetectorTool final : public GeoModelTool
{
public:

    // Standard constructor
    MuonDetectorTool(const std::string& type, const std::string& name, const IInterface* parent);

    // Standard destructor
    virtual ~MuonDetectorTool() override final;

    virtual StatusCode create() override final;
    virtual StatusCode clear() override final;

private:
    const ScintDD::MuonDetectorManager* m_manager;
    MuonGeoModelAthenaComps m_athenaComps;
    ServiceHandle< IGeoDbTagSvc > m_geoDbTagSvc;
    ServiceHandle< IRDBAccessSvc > m_rdbAccessSvc;
    ServiceHandle< IGeometryDBSvc > m_geometryDBSvc;
    StringProperty m_detectorName{this, "DetectorName", "Preshower"};
    BooleanProperty m_alignable{this, "Alignable", true};
    BooleanProperty m_useDynamicAlignFolders{this, "useDynamicAlignFolders", false};
    
};
#endif