from AthenaConfiguration.ComponentFactory import CompFactory

def MuonGeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )

    # if flags.Detector.GeometryMuon:
    #     MuonDetectorTool = CompFactory.MuonDetectorTool
    #     acc.getPrimary().DetectorTools += [ MuonDetectorTool() ]
    MuonDetectorTool = CompFactory.MuonDetectorTool
    acc.getPrimary().DetectorTools += [ MuonDetectorTool() ]
    return acc